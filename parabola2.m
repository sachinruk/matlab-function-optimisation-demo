function y=parabola2(x,a,b)
y=a*x.^2+b;