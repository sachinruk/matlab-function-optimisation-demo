function [y grad]=parabola_g(x)
y=x.^2;
grad=2*x;